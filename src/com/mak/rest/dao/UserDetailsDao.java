package com.mak.rest.dao;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import com.mak.rest.entity.UserBean;

public class UserDetailsDao {

	private String databaseFile = "C:\\Users\\kmanda\\WorkSpace-eclipse\\MAK_REST_SERVICE/UserDetailsFile.csv";
	private boolean fileAlreadyExist = new File(databaseFile).exists();
	
	
	public boolean addUser(UserBean userBean) {
		boolean addedUser= false;
		boolean userExist = validateUserLoginDetails(userBean.getUserName(), userBean.getPassword());
		if(!userExist) {
		
		try {
			
			CsvWriter csvWriter = new CsvWriter(new FileWriter(databaseFile,true),',');
			if(!fileAlreadyExist) {
				System.out.println("In dao in file not found code");
				csvWriter.write("FirstName");
				csvWriter.write("LastName");
				csvWriter.write("UserName");
				csvWriter.write("Mobile");
				csvWriter.write("Password");
				csvWriter.endRecord();
			}
			
			csvWriter.write(userBean.getFirstName());
			csvWriter.write(userBean.getLastName());
			csvWriter.write(userBean.getUserName());
			csvWriter.write(userBean.getMobile());
			csvWriter.write(userBean.getPassword());
			csvWriter.endRecord();
			csvWriter.close();
			
			addedUser = true;
			//System.out.println("After adding data to file");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		
		return addedUser;
		
	}



	public boolean validateUserLoginDetails(String userName, String password) {
		System.out.println("In Dao : start");
		boolean valid = false;
		try {
			CsvReader csvReader = new CsvReader(databaseFile);
			csvReader.readHeaders();
			while(csvReader.readRecord()) {
				if(csvReader.get("UserName").equals(userName) && csvReader.get("Password").equals(password)) {
					valid = true;
				}
			}
			System.out.println("after reading : "+valid);
			csvReader.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return valid;
	}
	
}
