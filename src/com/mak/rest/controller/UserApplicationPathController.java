package com.mak.rest.controller;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class UserApplicationPathController extends Application {

}
