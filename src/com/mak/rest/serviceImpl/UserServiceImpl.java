package com.mak.rest.serviceImpl;

import java.util.List;

import com.mak.rest.dao.UserDetailsDao;
import com.mak.rest.entity.UserBean;
import com.mak.rest.service.UserService;

public class UserServiceImpl implements UserService {

	private UserDetailsDao userDetailsDao = new UserDetailsDao();

	@Override
	public boolean validateUserLoginDetails(String userName, String password) {

		System.out.println("In serviceImpl class:before");
		
		boolean validUser = userDetailsDao.validateUserLoginDetails(userName, password);
		System.out.println("In serviceImpl class:after");

		return validUser;
	}

	@Override
	public String addUserDetails(UserBean user) {
		String addUserInfo = "";
		// System.out.println("In ServiceImpl before method");
		boolean addedUser = userDetailsDao.addUser(user);
		// System.out.println("In ServiceImpl after method return");
		if (addedUser) {
			addUserInfo = "The User " + user.getUserName() + " successfully signed up";
		} else {
			addUserInfo = "The UserName Already existing. Please try Forget Password";
		}
		return addUserInfo;
	}



}
