package com.mak.rest.serviceImpl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ServiceImplTest {
	private UserServiceImpl userServiceImpl;
	private String userName;
	private String password;


	@Before
	public void setUp() throws Exception {
		 userServiceImpl = new UserServiceImpl() ;
		 userName="krishna@gmail.com";
		 password= "krishnaa";
	}

	@Test
	public void testValidateUserLoginDetails() {
		boolean actual= userServiceImpl.validateUserLoginDetails(userName, password);
		boolean expected= true;
		assertEquals(expected,actual);
	}

}
